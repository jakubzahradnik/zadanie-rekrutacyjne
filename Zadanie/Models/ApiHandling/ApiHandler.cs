﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace Zadanie.Models
{
    public class ApiHandler
    {
        public Currency GetDataFromApi(string currencyCode, int howMuchData)
        {
            var client = new RestClient("http://api.nbp.pl/api/exchangerates/rates/a/");
            var request = new RestRequest(currencyCode + "/last/" + howMuchData + "/?format=json");
            var response = client.Execute(request);
            var deserialized = JsonConvert.DeserializeObject<Currency>(response.Content);
            return deserialized;
        }

        public class Currency
        {
            [JsonProperty(PropertyName = "currency")]
            public string CurrencyName { get; set; }

            [JsonProperty(PropertyName = "code")]
            public string Code { get; set; }

            [JsonProperty(PropertyName = "rates")]
            public List<Rate> Rates { get; set; }
        }

        public class Rate
        {
            [JsonProperty(PropertyName = "effectiveDate")]
            public string EffectiveDate { get; set; }

            [JsonProperty(PropertyName = "mid")]
            public double Mid { get; set; }
        }
    }
}