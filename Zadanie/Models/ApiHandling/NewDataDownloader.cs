﻿using System;
using System.Globalization;
using System.Linq;

namespace Zadanie.Models
{
    public class NewDataDownloader
    {
        public void DownaloadNewData(zadanieContext context)
        {
            var downloader = new DataDownloader(1);
            var newData = downloader.Download();

            foreach (var code in newData)
            {
                var hasData = context.Waluty.Any(c => c.Nazwa == code.Code);
                if (hasData) continue;
                var t = new Waluty { Nazwa = code.Code };
                context.Waluty.Add(t);
            }

            foreach (var details in newData)
            {
                foreach (var rates in details.Rates)
                {
                    var hasData = context.Szczegoly.Any(c => c.Data == Convert.ToDateTime(rates.EffectiveDate));
                    if (hasData) continue;
                    var t = new Szczegoly
                    {
                        Data = Convert.ToDateTime(rates.EffectiveDate),
                        Kurs = Convert.ToDecimal(rates.Mid),
                        Nazwa = details.Code
                    };
                    context.Szczegoly.Add(t);
                }
            }
            context.SaveChanges();
        }
    }
}