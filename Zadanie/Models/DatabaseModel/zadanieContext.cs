﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Zadanie.Models
{
    public partial class zadanieContext : DbContext
    {
        public zadanieContext()
        {
        }

        public zadanieContext(DbContextOptions<zadanieContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Szczegoly> Szczegoly { get; set; }
        public virtual DbSet<Waluty> Waluty { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=DESKTOP-2LNNRMG\\SQLEXPRESS;Database=zadanie;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Szczegoly>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Data).HasColumnType("date");

                entity.Property(e => e.Kurs).HasColumnType("money");

                entity.Property(e => e.Nazwa)
                    .IsRequired()
                    .HasMaxLength(3);

                entity.HasOne(d => d.NazwaNavigation)
                    .WithMany(p => p.Szczegoly)
                    .HasForeignKey(d => d.Nazwa)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Szczegoly_Waluty1");
            });

            modelBuilder.Entity<Waluty>(entity =>
            {
                entity.HasKey(e => e.Nazwa);

                entity.Property(e => e.Nazwa)
                    .HasMaxLength(3)
                    .ValueGeneratedNever();
            });
        }
    }
}