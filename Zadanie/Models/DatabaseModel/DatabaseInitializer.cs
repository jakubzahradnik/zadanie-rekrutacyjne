﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query;

namespace Zadanie.Models
{
    public class DatabaseInitializer
    {
        public void Seed()
        {
            var context = new zadanieContext();
            var data = new DataDownloader(200);
            var history = data.Download();

            foreach (var code in history)
            {
                var hasData = context.Waluty.Any(c => c.Nazwa == code.Code);
                if (hasData) continue;
                var t = new Waluty { Nazwa = code.Code };
                context.Waluty.Add(t);
            }

            foreach (var details in history)
            {
                foreach (var rates in details.Rates)
                {
                    var hasData = context.Szczegoly.Any(c => c.Data.ToString(CultureInfo.InvariantCulture) == rates.EffectiveDate);
                    if (hasData) continue;
                    var t = new Szczegoly
                    {
                        Data = Convert.ToDateTime(rates.EffectiveDate),
                        Kurs = Convert.ToDecimal(rates.Mid),
                        Nazwa = details.Code
                    };
                    context.Szczegoly.Add(t);
                }
            }
            context.SaveChanges();
        }
    }
}