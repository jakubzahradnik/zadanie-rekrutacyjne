﻿using System;
using System.Collections.Generic;

namespace Zadanie.Models
{
    public partial class Waluty
    {
        public Waluty()
        {
            Szczegoly = new HashSet<Szczegoly>();
        }

        public string Nazwa { get; set; }

        public virtual ICollection<Szczegoly> Szczegoly { get; set; }
    }
}
