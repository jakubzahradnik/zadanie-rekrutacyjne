﻿using System;
using System.Collections.Generic;

namespace Zadanie.Models
{
    public partial class Szczegoly
    {
        public int Id { get; set; }
        public decimal Kurs { get; set; }
        public DateTime Data { get; set; }
        public string Nazwa { get; set; }

        public virtual Waluty NazwaNavigation { get; set; }
    }
}
