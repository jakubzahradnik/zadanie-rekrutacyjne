﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zadanie.Models.EmailSender;

namespace Zadanie.Models
{
    public class Notification
    {
        private static zadanieContext _context;
        private readonly List<List<Szczegoly>> _query;
        private readonly List<Waluty> _currencies;

        public Notification(zadanieContext context)
        {
            _context = context;
            _query = new List<List<Szczegoly>>();
            _currencies = _context.Waluty.ToList();
        }

        public void CheckRun()
        {
            foreach (var currency in _currencies)
            {
                _query.Add(_context.Szczegoly.Where(p => p.Nazwa == currency.Nazwa).OrderByDescending(p => p.Data).Take(1).ToList());
            }

            CheckNewCurrencyValue();
            CheckBestCurrencyValueFrom30Days();
        }

        private void CheckNewCurrencyValue()
        {
            var fromApi = new DataDownloader(1);
            var newData = fromApi.Download();

            foreach (var queries in _query)
            {
                foreach (var query in queries)
                {
                    foreach (var currency in newData)
                    {
                        foreach (var VARIABLE in currency.Rates)
                        {
                            if (Convert.ToDateTime(VARIABLE.EffectiveDate) == query.Data) continue;
                            if (query.Kurs <= Convert.ToDecimal(VARIABLE.Mid)) continue;
                            //Email.SendMail(VARIABLE.Mid, currency.Code);
                        }
                    }
                }
            }
        }

        private void CheckBestCurrencyValueFrom30Days()
        {
            foreach (var currency in _currencies)
            {
                _query.Add(_context.Szczegoly.Where(p => p.Nazwa == currency.Nazwa).OrderByDescending(p => p.Data).Take(30).ToList());
            }

            var fromApi = new DataDownloader(1);
            var newData = fromApi.Download();

            foreach (var queries in _query)
            {
                foreach (var query in queries)
                {
                    foreach (var currency in newData)
                    {
                        foreach (var VARIABLE in currency.Rates)
                        {
                            if (Convert.ToDateTime(VARIABLE.EffectiveDate) == query.Data) continue;
                            if (query.Kurs <= Convert.ToDecimal(VARIABLE.Mid)) continue;
                            //Email.SendMail(VARIABLE.Mid, " w ciągu miesiąca " + currency.Code);
                        }
                    }
                }
            }
        }
    }
}