﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Zadanie.Models
{
    public class SettingsHandler
    {
        private string _path;

        public void SetPath(string path)
        {
            _path = path;
        }

        public UserSettings SettingsLoader()
        {
            var settings = JsonConvert.DeserializeObject<UserSettings>(File.ReadAllText(_path));
            return settings;
        }

        public AppSettings AppSettingsLoader()
        {
            var settings = JsonConvert.DeserializeObject<AppSettings>(File.ReadAllText(_path));
            return settings;
        }

        public class AppSettings
        {
            [JsonProperty(PropertyName = "downloadData")]
            public int DownloadData { get; set; }
        }

        public class UserSettings
        {
            public List<string> Currency { get; set; }
        }
    }
}