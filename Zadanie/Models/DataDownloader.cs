﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Microsoft.AspNetCore.ResponseCaching.Internal;
using Microsoft.IdentityModel.Tokens;

namespace Zadanie.Models
{
    public class DataDownloader
    {
        //private log4net.ILog log = log4net.LogManager.GetLogger(typeof(DataDownloader));
        private readonly string _currencySettings = new DirectoryInfo(Environment.CurrentDirectory).FullName + "\\Properties\\userSettings.json";

        private readonly string _appSettings = new DirectoryInfo(Environment.CurrentDirectory).FullName + "\\appsettings.json";
        private int _howMuchData;

        public DataDownloader(int howMuchData)
        {
            if (howMuchData > 254) howMuchData = 254;
            _howMuchData = howMuchData;
        }

        public void SetHowMuchData(int howMuchData) => _howMuchData = howMuchData;

        public List<ApiHandler.Currency> Download()
        {
            var currencies = new List<ApiHandler.Currency>();
            var settings = new SettingsHandler();
            settings.SetPath(_currencySettings);
            var userSettings = settings.SettingsLoader();
            settings.SetPath(_appSettings);
            var appSettings = settings.AppSettingsLoader();

            var api = new ApiHandler();

            foreach (var code in userSettings.Currency)
            {
                var data = api.GetDataFromApi(code, _howMuchData);
                currencies.Add(data);
            }

            return currencies;
        }
    }
}