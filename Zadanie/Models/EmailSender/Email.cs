﻿using FluentEmail.Core;

namespace Zadanie.Models.EmailSender
{
    public static class Email
    {
        public static void SendMail(double variableMid, string currencyCode)
        {
            IFluentEmail email = new FluentEmail.Core.Email();
            email
                .To("login@gmail.com")
                .Subject("Nowa cena" + currencyCode + " " + variableMid)
                .Send();
        }
    }
}