﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Zadanie.Models;

namespace Zadanie.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MyApiController : ControllerBase
    {
        private readonly zadanieContext _context;

        public MyApiController(zadanieContext context)
        {
            _context = context;
        }

        // GET: api/MyApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Szczegoly>>> GetSzczegoly()
        {
            return await _context.Szczegoly.ToListAsync();
        }

        //GET: /api/myapi/AUD?order=true
        [HttpGet("{id}")]
        public async Task<ActionResult<List<Szczegoly>>> GetSzczegoly(string id, bool order)
        {
            var szczegoly = new List<Szczegoly>();

            if (order == true)
            {
                szczegoly = await _context.Szczegoly.Where(p => p.Nazwa == id).OrderByDescending(p => p.Data)
                    .ToListAsync();
            }
            else
            {
                szczegoly = await _context.Szczegoly.Where(p => p.Nazwa == id).OrderBy(p => p.Data).ToListAsync();
            }

            if (szczegoly.Count == 0) return NotFound();

            return szczegoly;
        }

        // PUT: api/MyApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSzczegoly(int id, Szczegoly szczegoly)
        {
            if (id != szczegoly.Id)
            {
                return BadRequest();
            }

            _context.Entry(szczegoly).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SzczegolyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MyApi
        [HttpPost]
        public async Task<ActionResult<Szczegoly>> PostSzczegoly(Szczegoly szczegoly)
        {
            _context.Szczegoly.Add(szczegoly);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSzczegoly", new { id = szczegoly.Id }, szczegoly);
        }

        // DELETE: api/MyApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Szczegoly>> DeleteSzczegoly(int id)
        {
            var szczegoly = await _context.Szczegoly.FindAsync(id);
            if (szczegoly == null)
            {
                return NotFound();
            }

            _context.Szczegoly.Remove(szczegoly);
            await _context.SaveChangesAsync();

            return szczegoly;
        }

        private bool SzczegolyExists(int id)
        {
            return _context.Szczegoly.Any(e => e.Id == id);
        }
    }
}