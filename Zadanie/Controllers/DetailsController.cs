﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Zadanie.Models;

namespace Zadanie.Controllers
{
    public class DetailsController : Controller
    {
        private readonly zadanieContext _context;

        public DetailsController(zadanieContext context)
        {
            _context = context;
        }

        // GET: Details
        public async Task<IActionResult> Index()
        {
            var newData = new NewDataDownloader();
            newData.DownaloadNewData(_context);

            var notification = new Notification(_context);
            notification.CheckRun();

            var zadanieContext = _context.Szczegoly.Include(s => s.NazwaNavigation);
            //DatabaseInitializer.Seed();

            return View(await zadanieContext.ToListAsync());
        }

        // GET: Details/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var szczegoly = await _context.Szczegoly
                .Include(s => s.NazwaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (szczegoly == null)
            {
                return NotFound();
            }

            return View(szczegoly);
        }

        // GET: Details/Create
        public IActionResult Create()
        {
            ViewData["Nazwa"] = new SelectList(_context.Waluty, "Nazwa", "Nazwa");
            return View();
        }

        // POST: Details/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Kurs,Data,Nazwa")] Szczegoly szczegoly)
        {
            if (ModelState.IsValid)
            {
                _context.Add(szczegoly);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Nazwa"] = new SelectList(_context.Waluty, "Nazwa", "Nazwa", szczegoly.Nazwa);
            return View(szczegoly);
        }

        // GET: Details/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var szczegoly = await _context.Szczegoly.FindAsync(id);
            if (szczegoly == null)
            {
                return NotFound();
            }
            ViewData["Nazwa"] = new SelectList(_context.Waluty, "Nazwa", "Nazwa", szczegoly.Nazwa);
            return View(szczegoly);
        }

        // POST: Details/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Kurs,Data,Nazwa")] Szczegoly szczegoly)
        {
            if (id != szczegoly.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(szczegoly);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SzczegolyExists(szczegoly.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Nazwa"] = new SelectList(_context.Waluty, "Nazwa", "Nazwa", szczegoly.Nazwa);
            return View(szczegoly);
        }

        // GET: Details/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var szczegoly = await _context.Szczegoly
                .Include(s => s.NazwaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (szczegoly == null)
            {
                return NotFound();
            }

            return View(szczegoly);
        }

        // POST: Details/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var szczegoly = await _context.Szczegoly.FindAsync(id);
            _context.Szczegoly.Remove(szczegoly);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SzczegolyExists(int id)
        {
            return _context.Szczegoly.Any(e => e.Id == id);
        }
    }
}