Aplikacja zrezlizowana w ASP.NET Core 2.2, obsługa zadania pod kontrolerem Details (Controllers\DetailsController.cs).

Pobieram dane z API banku (tylko wybrane dane z użyciem JsonProperty) i zapisuję je w bazie MS SQL Server (Database First).
To, jakie waluty chcę pobrać zapisane jest w pliku userSettings.json (Zadanie\Zadanie\Properties\userSettings.json). Ten plik może być dowolnie modyfikowany, dane o nowej walucie są pobierane przy ponownym uruchomieniu programu. 

1. Baza danych, schemat w pliku jpg, pozwala na odnoszenie się do wszystkich rekordów danej waluty poprzez klucz nałożony na nazwę. Tabela "Waluty" zawiera informacje o tym, jakie waluty są przechowywane (Nazwa jest PK). Tabela szczegóły zawiera datę, kurs waluty oraz nazwę(FK).
2. Kod propogramu został przebudowany i przemyślany. W kodzie znajduje się inicjalizator bazy. 
3. Klasa służy inicjalizacji pustej bazy danymi pobranymi z API (do 255 dni wstecz).
4. Sprawdzenie nowych danych polega na pobraniu jednego rekordu z API, deserializacji oraz sprawdzeniu, czy daty pobranego wpisu oraz ostatniego wpisu w bazie są różne. Jeśli są nastąpi zapis nowych danych do bazy.
5. Powiadomieniem steruje klasa Notofication, która:
    a) dla najnowszego, wyższego kursu od "wczoraj" wysyła email
    b) dla najnowszego, wyższego kursu od 30 dni wysyła maila
    
6. Raport z czasu pracy: https://wakatime.com/@03591b39-198f-4cdc-9d14-ed55d129fb10/projects/skwiawrrdf?start=2019-04-22&end=2019-04-28
7. Schemat bazy danych znajduje się w pliku PNG.
8. Wystawiłem API ze szczegółami:
	a) /api/MyApi - zwraca wszystkie dane o walutach
	b) /api/MyApi/AUD?order=true - zwraca szczegóły wybranej waluty i sortuje po dacie od najnowszego wpisu do najstarszego. AUD to kod waluty, a order może przyjąć wartość false, wtedy API zwróci dane posortowane od najstarszych do najnowszych